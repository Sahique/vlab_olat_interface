# Use an official nginx image as the base image
FROM nginx:latest

# Copy the HTML file into the container's filesystem
COPY VLAB.html /usr/share/nginx/html/

# Expose port 80 to access the web server
EXPOSE 80
